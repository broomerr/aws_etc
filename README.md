# AWS Transcoder playground

## About

* Start transcoding of some video
* Wait for transcoding to finish (Wait for message through SNS topic/SQS queue)
* If Error - inform, if  success - publish message in SNS

## Assumptions

* AWS SDK configured on host
* in ~/.aws/config pointed region which has Transcoder available (not all regions has it)
* Transcoder pipeline and S3 buckets are pre-created and defined in  ETC_config.json 
* Preset_ID for output format defined in ETC_config.json 
* input_file_name and output_prefix defined in ETC_config.json ; as input key used basename of input_file_name
* sqs_queue_name defined in ETC_config.json ; created if needed and subscribed to SNS topic

## BUGS

* sns_search_for_topic_url() do not  search for topic, assume that there is the only topic and picks up that the only topic