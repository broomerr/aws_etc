
import logging
import os
import json
import boto3
from botocore.exceptions import ClientError



def create_elastic_transcoder_hls_job(pipeline_id, input_file,
                                      outputs, output_file_prefix):
    """Create an Elastic Transcoder HSL job

    :param pipeline_id: string; ID of an existing Elastic Transcoder pipeline
    :param input_file: string; Name of existing object in pipeline's S3 input bucket
    :param outputs: list of dictionaries; Parameters defining each output file
    :param output_file_prefix: string; Prefix for each output file name
    :param playlists: list of dictionaries; Parameters defining each playlist
    :return Dictionary containing information about the job
            If job could not be created, returns None
    """

    etc_client = boto3.client('elastictranscoder')
    try:
        response = etc_client.create_job(PipelineId=pipeline_id,
                                         Input={'Key': input_file},
                                         Outputs=outputs,
                                         OutputKeyPrefix=output_file_prefix)
    except ClientError as e:
        print(f'ERROR: {e}')
        return None
    return response['Job']

def check_if_file_transcoded(job_info):
    pass

def sns_publish_info_on_video(snsARN,inputKey, outputKey):
    sns_client = boto3.client('sns')
    json_message={"input": inputKey, "output": outputKey}
    message={'default': json.dumps(json_message) }
    print(message)
    #TODO need to check JSON encoding
    messageId=sns_client.publish(TopicArn=snsARN,
                       Message=json.dumps(message),
                       MessageStructure = 'json')
    print(f'messageId={messageId}')

def find_create_queue_url(sqs_gueue_name):
    sqs_client = boto3.client('sqs')
    response = sqs_client.get_queue_url(QueueName=sqs_gueue_name)
    print(f'get_queue_url response: {response}')
    if response is not None:
        results = [response['QueueUrl']]
    else:
                response = sqs.create_queue(QueueName=sqs_gueue_name,
                                            Attributes={
                                                'DelaySeconds': '60',
                                                'MessageRetentionPeriod': '86400',
                                                'ReceiveMessageWaitTimeSeconds': '20'  # let use long pooling
                                            })
                results = [response['QueueUrl']]
                print(f'results: {results}')
    if len(results) == 1:
        return(results[0])
    else:
        print(f'Found either multiple queues or none: {results}. Please change substring value for queue.')
        return(None)


def sns_subscribe_sqs_to_topic(snsARN,sqs_queue_name):
    sns_client = boto3.client('sns')
    sqs_queue_url=find_create_queue_url(sqs_queue_name)
    print(f'sqs_queue_url={sqs_queue_url}')
    sqs_client = boto3.client('sqs')
    response = sqs_client.get_queue_attributes(
                        QueueUrl=sqs_queue_url,
                        AttributeNames=['QueueArn']
                        )
    print(f'get_queue_attributes response={response}')

    response = sns_client.subscribe(
                                TopicArn=snsARN,
                                Protocol='sqs',
                                Endpoint=response['Attributes']['QueueArn'],
                                # ReturnSubscriptionArn=False
                                )
    print(f'subscribe response={response}')
    # response = sns_client.confirm_subscription(
    #                             TopicArn=snsARN,
    #                             Token='WHAT_is_here?',
    #                             AuthenticateOnUnsubscribe=False
    #                             )
    # print(f'confirm_subscription response={response}')



def sns_search_for_topic_url(topic_name_substr):
    sns_client = boto3.client('sns')
    response = sns_client.list_topics() # let pick up first one with
    print(f'list_topics response: {response}')
    if response['Topics'] is not None:
        results = [response['Topics']]
    else:
        return(None)

    #BUG We have for now just one  SNS topic, so skip searching!
    if len(results[0]) == 1:
        print(f'results[0]: {results[0][0]["TopicArn"]}')
        return(results[0][0]['TopicArn'])
    else:
        print(f'Found either multiple topics or none: {results}. Please change substring value for topics.')
        return(None)


def retrieve_sqs_messages(sqs_queue_url, num_msgs=1, wait_time=0, visibility_time=5):
    """Retrieve messages from an SQS queue

    The retrieved messages are not deleted from the queue.

    :param sqs_queue_url: String URL of existing SQS queue
    :param num_msgs: Number of messages to retrieve (1-10)
    :param wait_time: Number of seconds to wait if no messages in queue
    :param visibility_time: Number of seconds to make retrieved messages
        hidden from subsequent retrieval requests
    :return: List of retrieved messages. If no messages are available, returned
        list is empty. If error, returns None.
    """

    # Validate number of messages to retrieve
    if num_msgs < 1:
        num_msgs = 1
    elif num_msgs > 10:
        num_msgs = 10

    sqs_client = boto3.client('sqs')
    # Retrieve messages from an SQS queue
    try:
        msgs = sqs_client.receive_message(QueueUrl=sqs_queue_url,
                                          MaxNumberOfMessages=num_msgs,
                                          WaitTimeSeconds=wait_time,
                                          VisibilityTimeout=visibility_time)
    except ClientError as e:
        logging.error(e)
        return None

    # Return the list of retrieved messages
    # print(f'msgs={msgs}')
    if 'Messages' in msgs.keys():
        return msgs['Messages']
    else:
        return []


def delete_sqs_message(sqs_queue_url, msg_receipt_handle):
    """Delete a message from an SQS queue

    :param sqs_queue_url: String URL of existing SQS queue
    :param msg_receipt_handle: Receipt handle value of retrieved message
    """

    # Delete the message from the SQS queue
    sqs_client = boto3.client('sqs')
    sqs_client.delete_message(QueueUrl=sqs_queue_url,
                              ReceiptHandle=msg_receipt_handle)


def wait_for_job_result_in_sqs(sqs_queue_name,jobId):

    sqs_client = boto3.client('sqs')

    # Assign this value before running the program
    num_messages = 10
    sqs_queue_url = find_create_queue_url(sqs_queue_name)
    if sqs_queue_url is not None:
        logging.info(f'Found SQS queue: {sqs_queue_url}')
    else:
        logging.info(f'Could not found SQS queue.')
        exit(code=1)


    foundResult=0
    while foundResult==0:
        # Retrieve SQS messages
        msgs = retrieve_sqs_messages(sqs_queue_url, num_messages,20,20)
        if msgs is None:
            print(f'Some error happens!')
        elif len(msgs)==0:
            print(f'No messages found in queue!')
        else:
            for msg in msgs:
                logging.info(f'SQS: Message ID: {msg["MessageId"]}, '
                             f'Contents: {msg["Body"]}')
                receipt_handle = msg['ReceiptHandle']

                mmm=json.loads(msg["Body"])
                mmm2=json.loads(mmm["Message"])
                if "jobId" not in mmm2.keys():
                    logging.warn(f'Not job message. Skipping...')
                    break
                if mmm2["jobId"]!=jobId:
                    logging.warn(f'incorrecrt jobId {mmm2["jobId"]} found. Skipping...')
                    sqs_client.change_message_visibility(QueueUrl=sqs_queue_url,
                                                  ReceiptHandle=receipt_handle,
                                                  VisibilityTimeout=30)
                    print(f'Received and changed visibility timeout of message: {msg["MessageId"]}')
                    break
                logging.warn(f'found correct jobId {mmm2["jobId"]}')
                foundResult=1
                logging.warn(f'Transcoder: Subject: {mmm["Subject"]}')
                logging.warn(f'Transcoder: Message: {mmm["Message"]}')
                # time.sleep(25) #let wakeup a bit earlier then VisibilityTimeout

                # Remove the message from the queue
                if mmm2["state"]=='ERROR':
                    print(mmm["Subject"])
                    delete_sqs_message(sqs_queue_url, msg['ReceiptHandle'])
                else:
                    print(f'Message processed and  removed from quque: {msg["MessageId"]}')
                    delete_sqs_message(sqs_queue_url, msg['ReceiptHandle'])
                return(mmm2["state"])

def s3_upload_file(file_name,bucket_name):
    s3 = boto3.client('s3')
    s3.upload_file(file_name, bucket_name, os.path.basename(file_name))

def _main():
    logging.info("Load variable from  JSON config")
    configFile = "ETC_config.json"
    try:
        with  open(configFile) as f:
            configData = json.load(f)
    except IOError as er:
        print("IOError {0}: {1}".format(er.strerror,er.filename))

    s3_upload_file(configData["input_file_name"],configData["input_bucket"])

    sns_subscribe_sqs_to_topic(sns_search_for_topic_url('some_other_test?'),configData['sqs_queue_name'])

    # segment_duration='2' #seconds? Valid ony for ts or fmp4 PresetId

    outputs = [
        {
            'Key': 'test_transcoded.avi',
            'PresetId': configData['preset_id']
        }
        ]
    job_info = create_elastic_transcoder_hls_job(configData['pipeline_id'],
                                                 os.path.basename(configData['input_file_name']),
                                                 outputs, configData['output_prefix'])
    print(f'job_info={job_info}')
    if job_info is None:
        exit(1)
    else:
        result=wait_for_job_result_in_sqs(configData['sqs_queue_name'],job_info["Id"])
    if result!="ERROR":
        sns_publish_info_on_video(sns_search_for_topic_url('test'),job_info['Input']['Key'],job_info['Output']['Key'])


if __name__ == "__main__":
    _main()
